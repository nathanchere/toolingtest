defmodule NinjaPortal.Repo do
  use Ecto.Repo,
    otp_app: :ninja_portal,
    adapter: Ecto.Adapters.Postgres
end
