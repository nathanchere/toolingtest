defmodule NinjaPortalWeb.PageController do
  use NinjaPortalWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
