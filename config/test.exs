use Mix.Config

# Configure your database
config :ninja_portal, NinjaPortal.Repo,
  username: "postgres",
  password: "postgres",
  database: "ninja_portal_test",
  hostname: System.get_env("DB_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :ninja_portal, NinjaPortalWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
