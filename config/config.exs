# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :ninja_portal,
  ecto_repos: [NinjaPortal.Repo]

# Configures the endpoint
config :ninja_portal, NinjaPortalWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "kR/96uLzU4e//nEkPbpg55c5jKDTDM5lAk2Gb28El8E6XoWmyCLmdypX8v1b8pVf",
  render_errors: [view: NinjaPortalWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: NinjaPortal.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
